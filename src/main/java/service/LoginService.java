package service;

import java.util.Map;

import dao.LoginDao;

public class LoginService {

	LoginDao dao = new LoginDao();

	public boolean checkUserExists(Map<String, String> login) {
		boolean status = dao.login(login);
		return status;
	}
}
