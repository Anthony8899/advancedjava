package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.LoginService;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		Map<String, String> loginUser = new HashMap<>();
		loginUser.put("email", email);
		loginUser.put("pass", pass);
		LoginService service = new LoginService();
		boolean status = service.checkUserExists(loginUser);
		PrintWriter out = response.getWriter();
		if (status) {
//			request.setAttribute("email", pass);
//			RequestDispatcher rd = request.getRequestDispatcher("Home.jsp");
//
//			rd.forward(request, response);
			HttpSession sess = request.getSession();
			sess.setAttribute("email", email);

			response.sendRedirect("Home.jsp");
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			rd.forward(request, response);
		}

	}

}
