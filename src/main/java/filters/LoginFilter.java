package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/LoginFilter")
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		System.out.println(email + " : " + pass);
		if (email != "") {
			if (pass != "") {
				chain.doFilter(request, response);
			} else {
//				RequestDispatcher rd = request.getRequestDispatcher("Login.html");
//				rd.include(request, response);
				System.out.println("password is null");
			}
		} else {
//			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
//			rd.include(request, response);
			System.out.println("Email is required.");
		}

	}

}
