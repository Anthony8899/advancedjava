package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import static config.Configuration.*;

public class LoginDao {

	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public boolean login(Map<String, String> user) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("select * from users where email=? and pass=?");
			ps.setString(1, user.get("email"));
			ps.setString(2, user.get("pass"));
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println("Class Not Found Exception");
		} catch (SQLException e) {
			System.out.println("SQL Exception");
		}
		return false;
	}
}
